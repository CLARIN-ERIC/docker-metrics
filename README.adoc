= Metrics docker image
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

Split into separate images, see:

* https://gitlab.com/CLARIN-ERIC/docker-metrics-grafana
* https://gitlab.com/CLARIN-ERIC/docker-metrics-graphite
* https://gitlab.com/CLARIN-ERIC/docker-metrics-statsd
* https://gitlab.com/CLARIN-ERIC/docker-metrics-collectd